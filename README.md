# ZippyClient
zippyclient is a terminal client to use the service of zippyshare.com


## How to compile the project?

you need have installed maven 3 in your computer and use the next commands

```bash
mvn clean install
mvn clean package
```
After that you can go to /target folder and find the binary .jar 

## How to launch the program?

```bash
java -jar  zyppyClient-jar-with-dependencies.jar <the full path of your file>
 ``` 
  

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
