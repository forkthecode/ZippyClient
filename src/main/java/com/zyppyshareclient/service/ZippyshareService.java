package com.zyppyshareclient.service;

import com.zyppyshareclient.domain.Payload;
import okhttp3.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

public final class ZippyshareService {

    private final int MAX_SIZE = 500000000;//max size of file allowed, the size is in bytes
    private final String SERVICE_URL = "https://www61.zippyshare.com/upload";
    private final OkHttpClient OK_HTTP_CLIENT = new OkHttpClient();


    public Response uploadFile(Payload zippyObject) throws IOException {

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("uploadId", "HZE49EC5926330436C868475842CDEE6E5");
        addFiles(builder,zippyObject.getFiles());


        Request request = new Request.Builder()
                                .url(SERVICE_URL)
                                .post(builder.build())
                                .build();

        //Check the response
        Response response = OK_HTTP_CLIENT.newCall(request).execute();
        if (!response.isSuccessful())
            throw new IOException("Unexpected code " + response);

        return response;

    }


    private  void addFiles(MultipartBody.Builder builder, List<File> files){

        int counter = 1;
        for(File file: files){
            if(file.length()>MAX_SIZE){
                System.err.println(String.format("The file %s is too long",file.getName()));
            }

            builder.addFormDataPart("file_"+counter, file.getName(),
                    RequestBody.create(file,MediaType.parse("application/octet-stream")));
            counter++;
        }
    }
}
