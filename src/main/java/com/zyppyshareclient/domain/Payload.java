package com.zyppyshareclient.domain;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Payload {

    private List<File> files = new ArrayList<>();
    private boolean isPrivate;

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }
}
