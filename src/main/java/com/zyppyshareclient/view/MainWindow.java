package com.zyppyshareclient.view;

import com.zyppyshareclient.domain.Payload;
import com.zyppyshareclient.service.ZippyshareService;
import okhttp3.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainWindow {


    public static void main(String[] args) {

        List<File> files = new ArrayList<File>();
        if(args.length>0)
        {
            for(String arg:args){
                File file =  new File(arg);
                if(!file.exists()){
                    System.err.println(String.format("The file %s doesn't exits",arg));
                    continue;
                }
                files.add(file);
            }

            Payload myPayload = new Payload();
            myPayload.setFiles(files);
            ZippyshareService zippyService = new ZippyshareService();

            try (Response response = zippyService.uploadFile(myPayload)) {
                String[]  links = responseToStringList(response);

                for(String link: links){
                    System.out.println(link.trim());
                }

            } catch (IOException e) {
                System.err.println("The upload service has failed check it "+e);
            }
        }
        else{
            System.err.println("You need pass any file by parameter");
        }
    }

    private static String[]  responseToStringList(Response response){

        String[]  links = new String[0];
        try {
            Document doc = Jsoup.parse(response.body().string());
            Element element = doc.getElementById("plain-links");

            if(element.text()!=null && !element.text().isEmpty()){
                links = element.text().split(" ");
            }
        } catch (IOException e) {
            System.err.println("Error to process the html body "+e);
        }
        return links;
    }
}
